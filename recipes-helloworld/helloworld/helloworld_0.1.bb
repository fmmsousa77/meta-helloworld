SUMMARY = "Recipe for helloworld"
DESCRIPTION = "Simple helloworld program"
DEPENDS = "greetings"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=76dbe255ba9ea7b0058fd6d47fb76533"

BRANCH = "master"
SRCREV = "${AUTOREV}"

SRC_URI = "git://bitbucket.org/fmmsousa77/helloworld.git;branch=${BRANCH};protocol=ssh"

S = "${WORKDIR}/git"

inherit autotools

